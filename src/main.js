import Vue from 'vue'
import VueCustomElement from 'vue-custom-element'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.use(VueCustomElement)
Vue.config.productionTip = false
App.store = store
App.router = router

Vue.customElement('ssk-cart', App)
